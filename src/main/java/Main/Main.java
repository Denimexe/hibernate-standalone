package Main;

import Entity.AddressType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by denimexe on 10.12.17.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Hibernate tutorial");

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        
        List result = session.createQuery( "from AddressType" ).list();
        for ( AddressType AddressType : (List<AddressType>) result ) {
            System.out.println( "AddressType (" + AddressType.getId() + ") : " + AddressType.getName() );
        }
        session.getTransaction().commit();
        session.close();
    }

//    private static SessionFactory sessionFactory;
//
//    public static void main(String[] args) {
//        sessionFactory = new Configuration().configure().buildSessionFactory();
//
//        Session session = sessionFactory.openSession();
//        Transaction transaction = null;
//
//        transaction = session.beginTransaction();
//        List developers = session.createQuery("FROM AddressType ").list();
//
//        transaction.commit();
//        session.close();
//        for (Object at : developers) {
//            System.out.println(((AddressType) at).getName());
//        }
//        System.out.println("===================================");
//
//    }
//
//    public static List listDevelopers() {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = null;
//
//        transaction = session.beginTransaction();
//        List developers = session.createQuery("FROM AddressType ").list();
//
//        transaction.commit();
//        session.close();
//        return developers;
//    }

}
