package Main;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by denimexe on 10.12.17.
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactory;

    static {
        try {
            Configuration conf = new Configuration();
            sessionFactory = conf.configure().buildSessionFactory();
        } catch (Exception ex) {
            System.out.println("EXCEPTION: " + ex);
            throw  new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }
}
